Modelo de repositorio
=====================

[![pipeline status](https://gitlab.com/onacademy/modelo_de_repositorio/badges/master/pipeline.svg)](https://gitlab.com/onacademy/modelo_de_repositorio/-/commits/master)


Este es repositorio es un modelo base para utilizar en la resolución de los ejercicios de programación con Python.

En el repositorio se provee una configuración Vagrant lista para trabajar con Python 3 (v 3.6.9).

También se provee una configuración de integración continua para el motor de CI de GitLab. Esta configuración de CI build intenta biuldear todas los directorios del repositorio a excepción del directorio "personal".
