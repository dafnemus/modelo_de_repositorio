#!/bin/bash

set -e

pip3 install --user -r requirements.txt

for dir in */ ; do
  if [ $dir != "personal/" ]
  then
    cd $dir
    ./build.sh
    cd ..
  fi
done
