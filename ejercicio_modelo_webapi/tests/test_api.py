from app.api import api


def test_get():
    response = api.test_client().get('/')
    assert response.status_code == 200
    assert response.json['mensaje'] == 'hola!'
