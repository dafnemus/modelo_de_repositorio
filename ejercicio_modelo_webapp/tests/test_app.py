from datetime import date
from app.app import app


def test_index_contiene_fecha_actual():
    response = app.test_client().get('/')
    assert response.status_code == 200
    fecha_actual = date.today().strftime("%Y-%m-%d")
    assert fecha_actual in response.get_data(as_text=True)
